"use strict";

///////////////////////////////////////
// Modal window

const modal = document.querySelector(".modal");
const overlay = document.querySelector(".overlay");
const btnCloseModal = document.querySelector(".btn--close-modal");
const btnsOpenModal = document.querySelectorAll(".btn--show-modal");
const header = document.querySelector("header");

const openModal = function (e) {
  e.preventDefault();
  modal.classList.remove("hidden");
  overlay.classList.remove("hidden");
};

const closeModal = function () {
  modal.classList.add("hidden");
  overlay.classList.add("hidden");
};

btnsOpenModal.forEach((btn) => btn.addEventListener("click", openModal));

btnCloseModal.addEventListener("click", closeModal);
overlay.addEventListener("click", closeModal);

document.addEventListener("keydown", function (e) {
  if (e.key === "Escape" && !modal.classList.contains("hidden")) {
    closeModal();
  }
});

const cookiesMsg = document.createElement("div");
cookiesMsg.classList.add("cookie-message");
cookiesMsg.style.paddingRight = 0;
cookiesMsg.innerHTML =
  "We use cookies for improving functionality and analytics";
const btnCloseCookieMsg = document.createElement("button");
// btnCloseCookieMsg.classList.add("btn--show-modal");
// btnCloseCookieMsg.classList.add("btn");
btnCloseCookieMsg.classList.add("btn", "btn--show-modal");
btnCloseCookieMsg.textContent = "Got it!!!";
cookiesMsg.append(btnCloseCookieMsg);
header.prepend(cookiesMsg);

btnCloseCookieMsg.addEventListener("click", () => {
  cookiesMsg.remove();
});

cookiesMsg.style.backgroundColor = "#37383d";
cookiesMsg.style.width = "120%";
cookiesMsg.style.height =
  Number.parseFloat(getComputedStyle(cookiesMsg).height, 10) + 5 + "px";

const btnScrollTo = document.querySelector(".btn--scroll-to");
const section1 = document.querySelector("#section--1");
btnScrollTo.addEventListener("click", () => {
  // const s1Coords = section1.getBoundingClientRect();
  // window.scrollTo({
  //   left: s1Coords.left,
  //   top: s1Coords.top + window.pageYOffset,
  //   behavior: "smooth",
  // });
  section1.scrollIntoView({ behavior: "smooth" });
});

const nav = document.querySelector(".nav__links");
const navLinks = document.querySelectorAll(".nav__link");

nav.addEventListener("click", (e) => {
  e.preventDefault();
  const sectionHref = e.target?.getAttribute("href");
  const section = document.querySelector(sectionHref);
  if (section === null) return;
  section.scrollIntoView({ behavior: "smooth" });
});

const containerTabs = document.querySelector(".operations__tab-container");
const tabs = document.querySelectorAll(".operations__tab");
const contents = document.querySelectorAll(".operations__content");
containerTabs.addEventListener("click", (e) => {
  const tab = e.target.closest(".operations__tab");
  if (tab === null) return;
  tabs.forEach((tab) => tab.classList.remove("operations__tab--active"));
  tab.classList.add("operations__tab--active");
  // console.log(tab.dataset.tab);
  const content = document.querySelector(
    `.operations__content--${tab.dataset.tab}`
  );
  contents.forEach((c) => c.classList.remove("operations__content--active"));
  content.classList.add("operations__content--active");
});

const handleHoverMouseNav = (e, opacity) => {
  const tab = e.target.closest(".nav__item");
  if (tab === null) return;
  const allNavs = tab.closest(".nav__links").querySelectorAll(".nav__item");
  allNavs.forEach((nav) => {
    if (nav !== tab) nav.style.opacity = opacity;
  });
};

const containerNav = document.querySelector(".nav");
containerNav.addEventListener("mouseover", (e) => handleHoverMouseNav(e, 0.5));

containerNav.addEventListener("mouseout", (e) => handleHoverMouseNav(e, 1));

// const s1Coords = section1.getBoundingClientRect();

// window.addEventListener("scroll", () => {
//   // console.log(s1Coords.top);
//   if (window.scrollY > s1Coords.top) containerNav.classList.add("sticky");
//   else containerNav.classList.remove("sticky");
// });

const widthOfNav = -Number.parseFloat(getComputedStyle(containerNav).height);

const obsCallback = function (entries, observer) {
  const [entry] = entries;
  if (entry.intersectionRatio === 0) containerNav.classList.add("sticky");
  else containerNav.classList.remove("sticky");
  // containerNav.classList.add("sticky");
};

const obsOption = {
  root: null,
  threshold: 0,
  rootMargin: `${widthOfNav}px`,
};

const observerHeader = new IntersectionObserver(obsCallback, obsOption);
observerHeader.observe(header);

const sectionObsCallback = (entries, observer) => {
  const [entry] = entries;
  if (!entry.isIntersecting) return;
  entry.target.classList.remove("section--hidden");
  observer.unobserve(entry.target);
};

const sectionObserver = new IntersectionObserver(sectionObsCallback, {
  root: null,
  threshold: 0.2,
});

const sections = document.querySelectorAll(".section");
// console.log(sections);
sections.forEach((section) => {
  section.classList.add("section--hidden");
  sectionObserver.observe(section);
});

const imgsObserverCallback = function (entries, observer) {
  const [entry] = entries;
  // console.log(entry);
  if (!entry.isIntersecting) return;
  console.log(entry.target.src);
  entry.target.classList.remove("lazy-img");
  entry.target.src = entry.target.dataset.src;
  observer.unobserve(entry.target);
};
const imgObserver = new IntersectionObserver(imgsObserverCallback, {
  root: null,
  threshold: 0.75,
});
const imgs = document.querySelectorAll(".features__img");
imgs.forEach((img) => {
  imgObserver.observe(img);
});

const slides = document.querySelectorAll(".slide");
const slider = document.querySelector(".slider");
const containerDots = document.querySelector(".dots");

const createDots = function () {
  slides.forEach((slide, i) => {
    const html = `<button class="dots__dot" data-slide=${i + 1}></button>`;
    containerDots.insertAdjacentHTML("beforeend", html);
  });
};

createDots();

const dots = document.querySelectorAll(".dots__dot");

const moveToSlide = function (currSlide) {
  slides.forEach((slide, i) => {
    slide.style.transform = `translateX(${100 * (i - currSlide)}%)`;
  });
  dots.forEach((dot) => {
    console.log(dot);
    dot.classList.remove("dots__dot--active");
    if (Number(dot.dataset.slide) === currSlide + 1) {
      // console.log("AKSJAKSJKAJS");
      dot.classList.add("dots__dot--active");
    }
  });
};

moveToSlide(0);

const btnRight = document.querySelector(".slider__btn--right");
const btnLeft = document.querySelector(".slider__btn--left");

let currSlide = 0;
const maxSlides = slides.length;
btnRight.addEventListener("click", () => {
  if (currSlide === maxSlides - 1) {
    currSlide = 0;
  } else {
    currSlide++;
  }
  moveToSlide(currSlide);
});

btnLeft.addEventListener("click", () => {
  if (currSlide === 0) {
    currSlide = maxSlides - 1;
  } else {
    currSlide--;
  }
  moveToSlide(currSlide);
});

containerDots.addEventListener("click", (e) => {
  const dot = e.target.closest(".dots__dot");
  if (!dot) return;
  currSlide = dot.dataset.slide - 1;
  moveToSlide(currSlide);
});
