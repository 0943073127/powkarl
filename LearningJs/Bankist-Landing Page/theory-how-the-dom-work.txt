DOM is interface between JS code and browser (HTML document rendered by browser).
DOM is generated from a HTML element, which we can interact with.
DOM is a very complex API that contains a lot of methods and properties to interact with the DOM tree.

Every single node of the DOM tree is of the type node. (JS object)
Node type:
    - Element(<p>, <a>): has child node HTMLElement and it has one child type for different element(HTMLButtonElement, HTMLDivElement).
    - Text(<p>KHAKAHKAHKAH</p>).
    - Comment
    - Document
What makes everything work is inheritance
document is another type of node.  

How we can use eventListener,... : EventTarget is parent of node and window object(global objects, lot of methods and properties, many unrelated to DOM).

                                    EventTarget
                                    //                         \\
                                   Node                        Window
                        //       //      \\        \\
                    Element     Text     Comment   Document
                      ||
                    HTMLElement
                    //        \\
            HTMLButtonElement  HTMLDivElement
                
                      
Event: is basically a signal generated by a DOM node.
Advantage of addEventHandler: can be removed.

Event is generated:
  - First phase: capturing phase, propagate through parent node from top to bot.
  - Target phase.
  - Bubbling phase: move from bot to top.
  ==> This does not happen on all event. 