// let img;

const createImage = (imgPath) => {
  return new Promise((res, rej) => {
    const img = document.createElement("img");
    img.src = imgPath;
    console.log(imgPath, img);
    img.classList.add("images");

    img.addEventListener("load", () => {
      document.querySelector("body").appendChild(img);
      res(img);
    });

    img.addEventListener("error", (err) => {
      rej("Image does not exist");
    });
  });
};

const wait = (sec) => {
  return new Promise((res, _) => {
    setTimeout(res, sec * 1000);
  });
};

const loadNPause = async function () {
  const img1 = await createImage("img/img-1.jpg");
  console.log(img1);
  await wait(2);
  img.style.display = "none";
  const img2 = await createImage("img/img-2.jpg");
  console.log(img2);
  await wait(2);
  img.style.display = "none";
  const img3 = await createImage("img/img-3.jpg");
  console.log(img3);
  await wait(2);
  img.style.display = "none";
};

// loadNPause();

const loadAll = async function (imgArr) {
  const imgs = imgArr.map(async (i) => createImage(i));
  console.log(imgs);
  const imgsEl = await Promise.all(imgs);
  console.log(imgsEl);
  imgsEl.forEach((img) => img.classList.add("parallel"));
};

const imgArr = ["img/img-1.jpg", "img/img-2.jpg", "img/img-3.jpg"];
loadAll(imgArr);

// createImage("img/img-1.jpg")
//   .then((res) => {
//     console.log(res);
//     return wait(2);
//   })
//   .then((res) => {
//     img.style.display = "none";
//     return createImage("img/img-2.jpg");
//   })
//   .then((res) => {
//     console.log(res);
//     return wait(2);
//   })
//   .then((res) => {
//     img.style.display = "none";
//     return createImage("img/img-3.jpg");
//   })
//   .then((res) => {
//     console.log(res);
//     return wait(2);
//   })
//   .then((res) => (img.style.display = "none"))
//   .catch((err) => console.error(err));
