const wait = function (sec) {
  return new Promise((res, rej) => {
    setTimeout(res, sec * 1000);
  });
};

// wait(3).then(() => console.log("abc xyz"));
let img;

const createImage = function (imgPath) {
  return new Promise((res, rej) => {
    img = document.createElement("img");
    img.src = imgPath;
    img.addEventListener("load", () => {
      document.querySelector("body").appendChild(img);
      res(imgPath);
    });

    img.addEventListener("error", (err) => {
      rej(`Image does not exist`);
    });
  });
};

createImage("img/img-1.jpg")
  .then((res) => {
    console.log(res);
    return wait(2);
  })
  .then((res) => {
    img.style.display = "none";
    return createImage("img/img-2.jpg");
  })
  .then((res) => {
    console.log(res);
    return wait(2);
  })
  .then((res) => {
    img.style.display = "none";
    return createImage("img/img-3.jpg");
  })
  .then((res) => {
    console.log(res);
    return wait(2);
  })
  .then((res) => {
    img.style.display = "none";
  })
  .catch((err) => console.error(err));
