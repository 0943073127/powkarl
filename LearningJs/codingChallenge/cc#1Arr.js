const dogsJulia = [3, 5, 2, 12, 7];
const dogsKate = [4, 1, 15, 8, 3];

const checkDogs = (dogsJulia, dogsKate) => {
  const copyJulia = dogsJulia.slice(1, -2);
  const both = [...copyJulia, ...dogsKate];
  both.forEach((dog, i) => {
    console.log(
      `Dog number ${i + 1} is ${
        dog >= 3 ? `an adult, and is ${dog} years old` : "still a puppy"
      }`
    );
  });
};

checkDogs(dogsJulia, dogsKate);
