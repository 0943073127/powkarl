const dolphinScore1 = 97;
const dolphinScore2 = 112;
const dolphinScore3 = 81;

const koalasScore1 = 109;
const koalasScore2 = 95;
const koalasScore3 = 86;

const dolphinsAvg = (dolphinScore1 + dolphinScore2 + dolphinScore3) / 3;
const koalasAvg = (koalasScore1 + koalasScore2 + koalasScore3) / 3;

if (dolphinsAvg < koalasAvg && koalasAvg >= 100) {
  console.log("Koalas won");
} else if (dolphinsAvg > koalasAvg && dolphinsAvg >= 100) {
  console.log("Dolphins won");
} else if (dolphinsAvg === koalasAvg && dolphinsAvg >= 100) {
  console.log("Draw");
} else {
  console.log("No team won");
}
