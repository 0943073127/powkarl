const data = [5, 2, 4, 1, 15, 8, 3];
const data2 = [16, 6, 10, 5, 6, 1, 4];

const calcAverageHumanAge = (ages) => {
  const humanAges = ages.map((age) => (age <= 2 ? 2 * age : 16 + age * 4));
  console.log(humanAges);

  const dogOlderThan18 = humanAges.filter((age) => age >= 18);
  console.log(dogOlderThan18);

  const averageHumanAge =
    dogOlderThan18.reduce((acc, age) => acc + age, 0) / dogOlderThan18.length;

  console.log(averageHumanAge);
};

calcAverageHumanAge(data);
calcAverageHumanAge(data2);
