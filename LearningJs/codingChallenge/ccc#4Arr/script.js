const dogs = [
  { weight: 22, curFood: 250, owners: ["Alice", "Bob"] },
  { weight: 8, curFood: 200, owners: ["Matilda"] },
  { weight: 13, curFood: 275, owners: ["Sarah", "John"] },
  { weight: 32, curFood: 340, owners: ["Michael"] },
];

console.log("HELLO");
dogs.forEach((dog) => {
  dog.recommendedFood = dog.weight ** 0.75 * 28;
});
// console.log(dogs);

const sarahDog = dogs.find((dog) => dog.owners.includes("Sarah"));
if (sarahDog.curFood <= sarahDog.recommendedFood * 0.9)
  console.log(`Sarah's dog is eating too little`);
if (sarahDog.curFood >= sarahDog.recommendedFood * 1.1)
  console.log(`Sarah's dog is eating too much`);

const ownersEatTooMuch = dogs
  .filter((dog) => dog.curFood > dog.recommendedFood * 1.1)
  .map((dog) => dog.owners)
  .flat();

const ownersEatTooLittle = dogs
  .filter((dog) => dog.curFood < dog.recommendedFood * 0.9)
  .map((dog) => dog.owners)
  .flat();
console.log(ownersEatTooMuch);
console.log(ownersEatTooLittle);

console.log(`${ownersEatTooMuch.join(" and ")}'s dogs eat too much!`);
console.log(`${ownersEatTooLittle.join(" and ")}'s dogs eat too little!`);

const okayFood = (dog) =>
  dog.curFood >= dog.recommendedFood * 0.9 &&
  dog.curFood <= dog.recommendedFood * 1.1;

console.log(dogs.some((dog) => dog.curFood === dog.recommendedFood));
console.log(dogs.some(okayFood));

console.log(dogs.filter(okayFood));

const ascRecFood = dogs
  .slice()
  .sort((a, b) => a.recommendedFood - b.recommendedFood);
console.log(ascRecFood);
