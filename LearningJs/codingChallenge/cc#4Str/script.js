"use strict";

// Data needed for a later exercise
const flights =
  "_Delayed_Departure;fao93766109;txl2133758440;11:25+_Arrival;bru0943384722;fao93766109;11:45+_Delayed_Arrival;hel7439299980;fao93766109;12:05+_Departure;fao93766109;lis2323639855;12:30";

document.body.append(document.createElement("textarea"));
document.body.append(document.createElement("button"));

document.querySelector("button").addEventListener("click", () => {
  const contents = document.querySelector("textarea").value.split("\n");
  for (let id = 0; id < contents.length; id++) {
    let content = contents[id].toLowerCase().trim();
    let words = content.split("_");
    for (let i = 1; i < words.length; i++) {
      words[i] = words[i].replace(words[i][0], words[i][0].toUpperCase());
    }
    console.log(
      words
        .join("")
        .padEnd(20, " ")
        .concat("✅".repeat(id + 1))
    );
  }
});

const infos = flights.split("+");
for (let info of infos) {
  let [action, from, to, time] = info.split(";");

  let output = `${action.split("_").join(" ")} from ${from
    .slice(0, 3)
    .toUpperCase()} to ${to.slice(0, 3).toUpperCase()} (${time.replace(
    ":",
    "h"
  )})`;
  if (output.startsWith(" Delayed")) output = "🔴" + output;
  console.log(output.padStart(50, " "));
}
