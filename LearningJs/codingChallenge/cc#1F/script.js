"use strict";

const poll = {
  question: "What is your favorite programming language?",
  options: ["0: JavaScript", "1: Python", "2: Rust", "3: C++"],
  // This generates [0, 0, 0, 0]. More in the next section!
  answers: new Array(4).fill(0),

  registerNewAnswer() {
    const answer = Number(
      prompt(
        [this.question, ...this.options, "(Write option number)"].join("\n")
      )
    );
    if (![0, 1, 2, 3].includes(answer))
      alert("You have to input an option from 0 to 3");
    this.answers[answer]++;
    this.displayResults("array");
    this.displayResults("string");
  },

  displayResults: function (type) {
    if (type === "array") console.log(this.answers);
    else if (type === "string")
      console.log("Poll results are", ...this.answers);
  },
};

document
  .querySelector(".poll")
  .addEventListener("click", poll.registerNewAnswer.bind(poll));

const data1 = [5, 2, 3];
const data2 = [1, 5, 3, 9, 6, 1];

poll.displayResults.call({ answers: data1 }, "array");
poll.displayResults.call({ answers: data2 }, "string");

(function () {
  const header = document.querySelector("h1");
  header.style.color = "red";
  document.body.addEventListener("click", () => (header.style.color = "blue"));
})();
