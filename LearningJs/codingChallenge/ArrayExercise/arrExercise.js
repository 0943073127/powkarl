const account1 = {
  owner: "Jonas Schmedtmann",
  movements: [200, 450, -400, 3000, -650, -130, 70, 1300],
  interestRate: 1.2, // %
  pin: 1111,
};

const account2 = {
  owner: "Jessica Davis",
  movements: [5000, 3400, -150, -790, -3210, -1000, 8500, -30],
  interestRate: 1.5,
  pin: 2222,
};

const account3 = {
  owner: "Steven Thomas Williams",
  movements: [200, -200, 340, -300, -20, 50, 400, -460],
  interestRate: 0.7,
  pin: 3333,
};

const account4 = {
  owner: "Sarah Smith",
  movements: [430, 1000, 700, 50, 90],
  interestRate: 1,
  pin: 4444,
};

const accounts = [account1, account2, account3, account4];

const totalDeposit = accounts
  .map((acc) => acc.movements)
  .flat()
  .filter((mov) => mov > 0)
  .reduce((acc, mov) => acc + mov, 0);
console.log(totalDeposit);

const numDeposit1000 = accounts
  .map((acc) => acc.movements)
  .flat()
  .reduce((acc, mov) => (mov > 1000 ? acc + 1 : acc), 0);
console.log(numDeposit1000);

const depoANdWithdrawObj = accounts
  .map((acc) => acc.movements)
  .flat()
  .reduce(
    (acc, mov) => {
      mov > 0 ? (acc.deposit += mov) : (acc.withdrawal += Math.abs(mov));
      return acc;
    },
    { deposit: 0, withdrawal: 0 }
  );
console.log(depoANdWithdrawObj);

const convertTitleCase = (title) => {
  const exceptions = ["a", "an", "with", "but", "the", "or", "on", "in"];
  return title
    .toLowerCase()
    .split(" ")
    .map((word) =>
      !exceptions.includes(word)
        ? word.replace(word[0], word[0].toUpperCase())
        : word
    )
    .join(" ");
};
console.log(convertTitleCase("this is a nice title"));
console.log(convertTitleCase("this is a LONG title but not too long"));
console.log(convertTitleCase("and here is another title with an EXAMPLE"));
