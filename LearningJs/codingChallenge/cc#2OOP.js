class CarCl {
  constructor(make, speed) {
    this.make = make;
    this.speed = speed;
  }

  accelerate() {
    this.speed += 10;
    console.log(`${this.make} is going at ${this.speed}km/h`);
  }

  brake() {
    this.speed -= 5;
    console.log(`${this.make} is going at ${this.speed}km/h`);
  }

  get speedUS() {
    return this.speed / 1.6;
  }

  set speedUS(mile) {
    this.speed = mile * 1.6;
  }
}

const car = new CarCl("Ford", 120);
console.log(car.speedUS);
car.accelerate();
car.accelerate();
car.brake();

car.speedUS = 50;
console.log(car);
