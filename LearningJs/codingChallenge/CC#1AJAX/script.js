const whereAmI = function (lat, lng) {
  fetch(`https://geocode.xyz/${lat},${lng}?geoit=json`)
    .then((res) => res.json())
    .then((data) => {
      // console.log(data);
      if (data.success === false) throw new Error("You reload too fast");
      console.log(`You are in ${data.city}, ${data.country}`);
    })
    .catch((err) => console.log(err.message));
};

whereAmI(15.3289137, 108.7719882);
