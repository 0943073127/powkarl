const game = {
  team1: "Bayern Munich",
  team2: "Borrussia Dortmund",
  players: [
    [
      "Neuer",
      "Pavard",
      "Martinez",
      "Alaba",
      "Davies",
      "Kimmich",
      "Goretzka",
      "Coman",
      "Muller",
      "Gnarby",
      "Lewandowski",
    ],
    [
      "Burki",
      "Schulz",
      "Hummels",
      "Akanji",
      "Hakimi",
      "Weigl",
      "Witsel",
      "Hazard",
      "Brandt",
      "Sancho",
      "Gotze",
    ],
  ],
  score: "4:0",
  scored: ["Lewandowski", "Gnarby", "Lewandowski", "Hummels"],
  date: "Nov 9th, 2037",
  odds: {
    team1: 1.33,
    x: 3.25,
    team2: 6.5,
  },
};

for (let [i, scorer] of game.scored.entries()) {
  console.log(`Goal ${i + 1}: ${scorer}`);
}

let sum = 0;
for (let odd of Object.values(game.odds)) {
  sum += odd;
}
console.log(sum / Object.entries(game.odds).length);

for (let [team, odd] of Object.entries(game.odds)) {
  console.log(`Odd of victory ${game[team] || "draw"}: ${odd}`);
}

const scorers = {};
for (let scorer of Object.values(game.scored)) {
  if (scorers[scorer]) scorers[scorer]++;
  else scorers[scorer] = 1;
}
console.log(scorers);
