"use strict";

let myNumber = Math.trunc(Math.random() * 20) + 1;
let currentScore = 20;

document.querySelector(".check").addEventListener("click", () => {
  let guessNumber = document.querySelector(".guess").value;
  if (guessNumber === "") {
    alert("Please input a number");
    return;
  }
  guessNumber = Number(guessNumber);
  if (guessNumber < 1 || guessNumber > 20) {
    alert("Input a number from 1 to 20");
    return;
  }
  if (currentScore > 0)
    if (guessNumber < myNumber) {
      document.querySelector(".message").textContent = "Too Low";
      document.querySelector(".score").textContent = --currentScore;
    } else if (guessNumber > myNumber) {
      document.querySelector(".message").textContent = "Too High";
      document.querySelector(".score").textContent = --currentScore;
    } else {
      document.querySelector(".number").textContent = myNumber;
      document.querySelector(".message").textContent = "Correct Number";
      document.querySelector(".highscore").textContent =
        currentScore > +document.querySelector(".highscore").textContent
          ? currentScore
          : +document.querySelector(".highscore").textContent;
      document.body.style.background = "#60b347";
      document.querySelector(".number").style.width = "30rem";
    }
  else {
    document.querySelector(".message").textContent = "You lost the game!!!";
  }
});

document.querySelector(".again").addEventListener("click", () => {
  currentScore = 20;
  myNumber = Math.trunc(Math.random() * 20) + 1;
  document.querySelector(".message").textContent = "Start guessing...";
  document.querySelector(".number").textContent = "?";
  document.querySelector(".score").textContent = "20";
  document.querySelector(".guess").value = "";
  document.body.style.background = "#222";
  document.querySelector(".number").style.width = "15rem";
});
