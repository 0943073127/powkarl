"use strict";

const btnNewGame = document.querySelector(".btn--new");
const btnRoll = document.querySelector(".btn--roll");
const btnHold = document.querySelector(".btn--hold");
const player1 = document.querySelector(".player--0");
const scorePlayer1 = document.querySelector("#score--0");
const currScorePlayer1 = document.querySelector("#current--0");
const player2 = document.querySelector(".player--1");
const scorePlayer2 = document.querySelector("#score--1");
const currScorePlayer2 = document.querySelector("#current--1");
const imgDice = document.querySelector(".dice");

imgDice.style.display = "none";

let playerActive = player1;
let isPlaying = true;
let currScore = 0;

btnNewGame.addEventListener("click", () => {
  scorePlayer1.textContent = 0;
  currScorePlayer1.textContent = 0;
  scorePlayer2.textContent = 0;
  currScorePlayer2.textContent = 0;
  imgDice.style.display = "none";
  player1.classList.remove("player--winner");
  player2.classList.remove("player--winner");
  player1.classList.add("player--active");
  player2.classList.remove("player--active");
  playerActive = player1;
  isPlaying = true;
  currScore = 0;
});

btnRoll.addEventListener("click", () => {
  if (isPlaying) {
    const diceNumber = Math.trunc(Math.random() * 6) + 1;
    imgDice.src = `dice-${diceNumber}.png`;
    imgDice.style.display = "block";
    if (diceNumber !== 1) {
      currScore += diceNumber;
      playerActive.children[2].children[1].textContent = currScore;
    } else {
      currScore = 0;
      playerActive.classList.remove("player--active");
      playerActive.children[2].children[1].textContent = currScore;
      if (playerActive === player1) playerActive = player2;
      else playerActive = player1;
      playerActive.classList.add("player--active");
    }
  }
});

const winner = (player) => {
  player.classList.add("player--winner");
};

btnHold.addEventListener("click", () => {
  if (isPlaying) {
    const score = +playerActive.children[1].textContent;
    playerActive.children[1].textContent = score + currScore;
    if (score + currScore >= 100) {
      winner(playerActive);
      isPlaying = false;
    }
    currScore = 0;
    playerActive.children[2].children[1].textContent = currScore;
    playerActive.classList.remove("player--active");
    if (playerActive === player1) playerActive = player2;
    else playerActive = player1;
    playerActive.classList.add("player--active");
  }
});
