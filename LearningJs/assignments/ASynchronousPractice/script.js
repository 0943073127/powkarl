// navigator.geolocation.getCurrentPosition(
//   (position) => {
//     const lat = position.coords.latitude;
//     const lng = position.coords.longitude;
//     console.log(lat, lng);
//   },
//   (err) => {
//     console.error(err);
//   }
// );

const logPosition = new Promise((res, rej) => {
  navigator.geolocation.getCurrentPosition(
    (position) => res(position),
    (err) => rej(err)
  );
});

logPosition
  .then((res) => {
    const lat = res.coords.latitude;
    const lng = res.coords.longitude;
    console.log(lat, lng);
  })
  .catch((err) => console.error(err));
