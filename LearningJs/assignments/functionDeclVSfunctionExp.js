function percentageOfWorld1(population) {
  return (population / 7900) * 100;
}

console.log(percentageOfWorld1(1440));
console.log(percentageOfWorld1(100));
console.log(percentageOfWorld1(330));

const percentageOfWorld2 = function (population) {
  return (population / 7900) * 100;
};
console.log(percentageOfWorld2(1440));
console.log(percentageOfWorld2(100));
console.log(percentageOfWorld2(330));
