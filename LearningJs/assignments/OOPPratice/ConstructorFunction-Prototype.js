const Person = function (name, age, office) {
  this.name = name;
  this.age = age;
  this.office = office;
};

Person.prototype.displayInfo = function () {
  console.log(
    `My name is ${this.name}, I'm ${this.age} years old and studying at ${this.office}`
  );
};

const kha = new Person("Bao Kha", 21, "HCMUT");
// create new empty object {}
// function Person is called, this = {}
// link {} to prototype
// return {}

const chi = new Person("Linh Chi", 21, "HCMUS");

console.log(kha);
kha.displayInfo();
console.log(chi);
chi.displayInfo();

let a = "a";
console.log(kha instanceof Person);
console.log(a instanceof Person);

console.dir(kha);
