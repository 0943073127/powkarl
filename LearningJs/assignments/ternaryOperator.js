const country = "Vietnam"; // const
let population = 34; // can be changed

console.log(
  `${country}'s population is ${population > 33 ? "above" : "below"} average`
);
