const isIsland = false; // const
const language = "Vietnamese"; // const

const country = "Vietnam"; // const
let population = 100; // can be changed
const continent = "Asia"; // const

//using template literals
console.log(
  `${country} is in ${continent}, and its ${population} million people speak ${language}`
);
