const isIsland = false; // const
const language = "English"; // const

const country = "Vietnam"; // const
let population = 30; // can be changed
const continent = "Asia"; // const

if (population < 50 && language === "English" && !isIsland) {
  console.log(`You should live in ${country}`);
} else {
  console.log(`${country} does not meet your criteria`);
}
