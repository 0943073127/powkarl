const percentageOfWorld = (population) => (population / 7900) * 100;

const describePopulation = function (country, population) {
  return `${country} has ${population} million people, which is about ${percentageOfWorld(
    population
  )}% of the world.`;
};
console.log(describePopulation("China", 1441));
