const isIsland = false; // const
const language = "Vietnamese"; // const

const country = "Vietnam"; // const
let population = 100; // can be changed
const continent = "Asia"; // const

//1
console.log(population / 2);

//2
console.log(++population);

//3
console.log(population > 6);

//4
console.log(population < 33);

//5
console.log(
  `${country} is in ${continent}, and its ${population} people speak ${language}`
);
