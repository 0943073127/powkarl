console.log("Exporting module...");

const arr = [];
const add = function (el) {
  arr.push(el);
};

const displayArr = function () {
  console.log(arr);
};

export {arr, add, displayArr};