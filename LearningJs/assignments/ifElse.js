const isIsland = false; // const
const language = "Vietnamese"; // const

const country = "Vietnam"; // const
let population = 130; // can be changed
const continent = "Asia"; // const

if (population > 33) {
  console.log(`${country}'s population is above average`);
} else {
  console.log(
    `${country}'s population is ${33 - population} million below average`
  );
}
