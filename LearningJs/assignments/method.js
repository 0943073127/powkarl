const myCountry = {
  country: "Vietnam",
  capital: "Hanoi",
  language: "Vietnamese",
  population: 100,
  neighbors: ["Laos", "Cambodia", "China"],
  describe: function () {
    return `${this.country} has ${this.population} million ${this.language}-speaking people, ${this.neighbors.length} countries and a capital called ${this.capital}`;
  },
  checkIsIsland: function () {
    this.isIsland = this.neighbors.length > 0 ? false : true;
  },
};

console.log(myCountry.describe());
myCountry.checkIsIsland();
console.log(myCountry.isIsland);
