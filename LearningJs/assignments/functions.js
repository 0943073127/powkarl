function describeCountry(country, population, capitalCity) {
  return `${country} has ${population} million people and its capital city is ${capitalCity}`;
}

const vietnam = describeCountry("Vietnam", 100, "Hanoi");
const finland = describeCountry("Finland", 6, "Helsinki");
const usa = describeCountry("USA", 300, "Washington DC");

console.log(vietnam);
console.log(finland);
console.log(usa);
