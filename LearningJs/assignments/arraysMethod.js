const neighbors = ["Laos", "Cambodia", "China"];

neighbors.push("Utopia");
console.log(neighbors);
neighbors.pop();
console.log(neighbors);

if (!neighbors.includes("Germany")) {
  console.log("Properly not a central European country");
}

neighbors[neighbors.indexOf("China")] = "People Republic Of China";
console.log(neighbors);
