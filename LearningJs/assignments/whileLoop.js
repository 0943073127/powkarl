const populations = [100, 1441, 330, 1300];
const percentageOfWorld = (population) => (population / 7900) * 100;

const percentages = [];
let i = 0;
while (i < populations.length) {
  percentages.push(percentageOfWorld(populations[i]));
  i++;
}
console.log(percentages);
