const populations = [100, 1441, 330, 1300];
populations.length === 4 ? console.log("TRUE") : console.log("FALSE");

const percentageOfWorld = (population) => (population / 7900) * 100;
const percentages = [
  percentageOfWorld(populations[0]),
  percentageOfWorld(populations[1]),
  percentageOfWorld(populations[2]),
  percentageOfWorld(populations[3]),
];
console.log(percentages);
