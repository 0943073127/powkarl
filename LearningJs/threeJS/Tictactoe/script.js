import { FontLoader } from "./FontLoader.js";
import * as THREE from "./node_modules/three/build/three.module.js";
import { OrbitControls } from "./OrbitControls.js";
import { TextGeometry } from "./TextGeometry.js";

const canvas = document.querySelector("canvas");
const h1 = document.querySelector("h1");
const h6 = document.querySelector("h6");

const renderer = new THREE.WebGL1Renderer({ canvas });
renderer.setSize(window.innerWidth, window.innerHeight);

const fov = 75;
const aspect = 2;
const far = 100;
const near = 0.1;
const camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
camera.position.z = 10;
const scene = new THREE.Scene();
scene.background = new THREE.Color("white");

const cellWidth = 1;
const cellHeight = 1;
const cellDepth = 0.1;
const geometry = new THREE.BoxGeometry(cellWidth, cellHeight, cellDepth);
const board = new THREE.Group();

let cell;
for (let i = -5; i < 5; i++) {
  for (let j = -3; j < 7; j++) {
    if (i % 2 === 0) {
      if (j % 2 === 0) {
        const whiteMaterial = new THREE.MeshBasicMaterial({ color: 0xeff9f0 });
        cell = new THREE.Mesh(geometry, whiteMaterial);
      } else {
        const darkMaterial = new THREE.MeshBasicMaterial({ color: 0xddc8c4 });
        cell = new THREE.Mesh(geometry, darkMaterial);
      }
    } else {
      if (j % 2 !== 0) {
        const whiteMaterial = new THREE.MeshBasicMaterial({ color: 0xeff9f0 });
        cell = new THREE.Mesh(geometry, whiteMaterial);
      } else {
        const darkMaterial = new THREE.MeshBasicMaterial({ color: 0xddc8c4 });
        cell = new THREE.Mesh(geometry, darkMaterial);
      }
    }
    cell.position.set(i, j, 0);
    board.add(cell);
  }
}
board.rotation.x = -0.5;

scene.add(board);

const controls = new OrbitControls(camera, renderer.domElement);
let pickedCell;

class PickHelper {
  constructor() {
    this.raycaster = new THREE.Raycaster();
    this.pickedObject = null;
    this.pickedObjectSavedColor = 0;
  }
  pick(normalizedPosition, scene, camera, time) {
    // restore the color if there is a picked object
    if (this.pickedObject) {
      this.pickedObject.material.color.setHex(this.pickedObjectSavedColor);
      this.pickedObject = undefined;
    }

    // cast a ray through the frustum
    this.raycaster.setFromCamera(normalizedPosition, camera);
    // get the list of objects the ray intersected
    const intersectedObjects = this.raycaster.intersectObjects(scene.children);
    if (intersectedObjects.length) {
      // pick the first object. It's the closest one
      this.pickedObject = intersectedObjects[0].object;

      pickedCell = this.pickedObject;

      // save its color
      this.pickedObjectSavedColor = this.pickedObject.material.color.getHex();
      // set its emissive color to flashing red/yellow
      this.pickedObject.material.color.setHex(
        (time * 8) % 2 > 1 ? 0xffff00 : 0xff0000
      );
    }
  }
}

const pickPosition = { x: 0, y: 0 };
clearPickPosition();

function getCanvasRelativePosition(event) {
  const rect = canvas.getBoundingClientRect();
  return {
    x: ((event.clientX - rect.left) * canvas.width) / rect.width,
    y: ((event.clientY - rect.top) * canvas.height) / rect.height,
  };
}

function setPickPosition(event) {
  const pos = getCanvasRelativePosition(event);
  pickPosition.x = (pos.x / canvas.width) * 2 - 1;
  pickPosition.y = (pos.y / canvas.height) * -2 + 1; // note we flip Y
}

function clearPickPosition() {
  // unlike the mouse which always has a position
  // if the user stops touching the screen we want
  // to stop picking. For now we just pick a value
  // unlikely to pick something
  pickPosition.x = -100000;
  pickPosition.y = -100000;
}

const findCell = (cellId) =>
  board.children.findIndex((cell) => cell.id === cellId);

let turn = 1;

const findDiagonalDown = (index, n) => {
  let k = 0;
  while (index % 10 !== 0 && index > 10) {
    index -= n;
    k++;
  }
  return k;
};

const findDiagonalUp = (index, n) => {
  let k = 0;
  while (index / 10 < 9 && (index + 1) % 10 !== 0) {
    index += n;
    k++;
  }
  return k;
};

const findDiagonalDownLTR = (index, n) => {
  let k = 0;
  while (index % 10 !== 0 && index / 10 < 9) {
    index += n;
    k++;
  }
  return k;
};

const findDiagonalUpRTL = (index, n) => {
  let k = 0;
  while (index > 10 && (index + 1) % 10 !== 0) {
    index -= n;
    k++;
  }
  return k;
};

h6.addEventListener("click", () => {
  for (let i = 0; i < 100; i++) {
    board.children[i].children.pop();
    board.children[i].hasPiece = false;
    board.children[i].pieceContent = undefined;
  }
  turn = 1;
  h1.textContent = "";
  h6.style.display = "none";
});

const checkWin = function (pickedCell) {
  const index = findCell(pickedCell.id);
  let starter, ender;
  const content = board.children[index].pieceContent;
  let isWin;
  if (index % 10 < 4) {
    starter = Math.trunc(index / 10) * 10;
    ender = index;
    for (let i = starter; i <= ender; i++) {
      isWin = true;
      for (let j = i; j <= i + 4; j++) {
        if (!board.children[j] || board.children[j].pieceContent !== content) {
          isWin = false;
          break;
        }
      }
      if (isWin) {
        h1.textContent = `PLAYER ${turn} WIN !!!!`;
        h6.style.display = "block";
        h6.textContent = "Play again";
        break;
      }
    }
  } else if (index % 10 > 5) {
    starter = Math.trunc(index / 10 + 1) * 10;
    ender = index;
    for (let i = starter; i >= ender; i--) {
      isWin = true;
      for (let j = i; j >= i - 4; j--) {
        if (!board.children[j] || board.children[j].pieceContent !== content) {
          isWin = false;
          break;
        }
      }
      if (isWin) {
        h1.textContent = `PLAYER ${turn} WIN !!!!`;
        h6.style.display = "block";
        h6.textContent = "Play again";
        break;
      }
    }
  } else {
    starter = index - 4;
    ender = index + 4;
    for (let i = starter; i <= ender; i++) {
      isWin = true;
      for (let j = i; j <= i + 4; j++) {
        if (!board.children[j] || board.children[j].pieceContent !== content) {
          isWin = false;
          break;
        }
      }
      if (isWin) {
        h1.textContent = `PLAYER ${turn} WIN !!!!`;
        h6.style.display = "block";
        h6.textContent = "Play again";
        break;
      }
    }
  }
  if (!isWin) {
    if (Math.trunc(index / 10) < 4) {
      starter = index % 10;
      ender = index;
      for (let i = starter; i <= ender; i += 10) {
        isWin = true;
        for (let j = i; j <= i + 40; j += 10) {
          if (
            !board.children[j] ||
            board.children[j].pieceContent !== content
          ) {
            isWin = false;
            break;
          }
        }
        if (isWin) {
          h1.textContent = `PLAYER ${turn} WIN !!!!`;
          h6.style.display = "block";
          h6.textContent = "Play again";
          break;
        }
      }
    } else if (Math.trunc(index / 10) > 5) {
      starter = 90 + (index % 10);
      ender = index;
      for (let i = starter; i >= ender; i -= 10) {
        isWin = true;
        for (let j = i; j >= i - 40; j -= 10) {
          if (
            !board.children[j] ||
            board.children[j].pieceContent !== content
          ) {
            isWin = false;
            break;
          }
        }
        if (isWin) {
          h1.textContent = `PLAYER ${turn} WIN !!!!`;
          h6.style.display = "block";
          h6.textContent = "Play again";
          break;
        }
      }
    } else {
      starter = index - 40;
      ender = index;
      for (let i = starter; i <= ender; i += 10) {
        isWin = true;
        for (let j = i; j <= i + 40; j += 10) {
          if (
            !board.children[j] ||
            board.children[j].pieceContent !== content
          ) {
            isWin = false;
            break;
          }
        }
        if (isWin) {
          h1.textContent = `PLAYER ${turn} WIN !!!!`;
          h6.style.display = "block";
          h6.textContent = "Play again";
          break;
        }
      }
    }
  }
  if (!isWin) {
    console.log("KHAAASAS");
    if (findDiagonalDown(index, 11) < 4) {
      let k = findDiagonalDown(index, 11);
      starter = index - k * 11;
      ender = index;
      for (let i = starter; i <= ender; i += 11) {
        isWin = true;
        for (let j = i; j <= i + 44; j += 11) {
          console.log(`J : ${j}`);
          if (
            !board.children[j] ||
            board.children[j].pieceContent !== content
          ) {
            isWin = false;
            break;
          }
        }
        if (isWin) {
          h1.textContent = `PLAYER ${turn} WIN !!!!`;
          h6.style.display = "block";
          h6.textContent = "Play again";
          break;
        }
      }
    } else if (findDiagonalUp(index, 11) < 4) {
      let k = findDiagonalUp(index, 11);
      starter = index + k * 11;
      ender = index;
      for (let i = starter; i >= ender; i -= 11) {
        isWin = true;
        for (let j = i; j >= i - 44; j -= 11) {
          if (
            !board.children[j] ||
            board.children[j].pieceContent !== content
          ) {
            isWin = false;
            break;
          }
        }
        if (isWin) {
          h1.textContent = `PLAYER ${turn} WIN !!!!`;
          h6.style.display = "block";
          h6.textContent = "Play again";
          break;
        }
      }
    } else {
      starter = index - 44;
      ender = index;
      for (let i = starter; i <= ender; i += 11) {
        isWin = true;
        for (let j = i; j <= i + 44; j += 11) {
          if (
            !board.children[j] ||
            board.children[j].pieceContent !== content
          ) {
            isWin = false;
            break;
          }
        }
        if (isWin) {
          h1.textContent = `PLAYER ${turn} WIN !!!!`;
          h6.style.display = "block";
          h6.textContent = "Play again";
          break;
        }
      }
    }
  }
  if (!isWin) {
    console.log(findDiagonalDownLTR(index, 9), findDiagonalUpRTL(index, 9));
    if (findDiagonalDownLTR(index, 9) < 4) {
      let k = findDiagonalDownLTR(index, 9);
      starter = index + k * 9;
      ender = index;
      for (let i = starter; i >= ender; i -= 9) {
        isWin = true;
        for (let j = i; j >= i - 36; j -= 9) {
          if (
            !board.children[j] ||
            board.children[j].pieceContent !== content
          ) {
            isWin = false;
            break;
          }
        }
        if (isWin) {
          h1.textContent = `PLAYER ${turn} WIN !!!!`;
          h6.textContent = "Play again";
          break;
        }
      }
    } else if (findDiagonalUpRTL(index, 9) < 4) {
      let k = findDiagonalUpRTL(index, 9);
      starter = index - k * 9;
      ender = index;
      for (let i = starter; i <= ender; i += 9) {
        isWin = true;
        for (let j = i; j <= i + 36; j += 9) {
          if (
            !board.children[j] ||
            board.children[j].pieceContent !== content
          ) {
            isWin = false;
            break;
          }
        }
        if (isWin) {
          h1.textContent = `PLAYER ${turn} WIN !!!!`;
          h6.textContent = "Play again";
          break;
        }
      }
    } else {
      console.log("KHA");
      starter = index - 36;
      ender = index;
      for (let i = starter; i <= ender; i += 9) {
        isWin = true;
        for (let j = i; j <= i + 36; j += 9) {
          if (
            !board.children[j] ||
            board.children[j].pieceContent !== content
          ) {
            isWin = false;
            break;
          }
        }
        if (isWin) {
          h1.textContent = `PLAYER ${turn} WIN !!!!`;
          h6.textContent = "Play again";
          break;
        }
      }
    }
  }
};

const putPiece = function (event) {
  let XO = turn === 1 ? "X" : "O";
  let color = XO === "X" ? 0xff0000 : 0x68bb59;
  const loader = new FontLoader();
  loader.load("helvetiker_bold.typeface.json", function (font) {
    const pieceGeo = new TextGeometry(XO, {
      font: font,
      size: 0.5,
      height: 0.75,
    });
    const pieceMat = new THREE.MeshBasicMaterial({ color: color });
    const piece = new THREE.Mesh(pieceGeo, pieceMat);
    piece.position.set(-0.25, -0.325, 0.1);
    piece.hasPiece = true;
    if (pickedCell !== undefined && !pickedCell.hasPiece) {
      pickedCell.add(piece);
      pickedCell.hasPiece = true;
      pickedCell.pieceContent = XO;
      checkWin(pickedCell);
      turn = turn === 1 ? 2 : 1;
    }
    console.log(board);
    pickedCell = undefined;
  });
};

window.addEventListener("mousemove", setPickPosition);
window.addEventListener("mouseout", clearPickPosition);
window.addEventListener("mouseleave", clearPickPosition);
window.addEventListener("click", putPiece);

const pickHelper = new PickHelper();

const animate = function (time) {
  time *= 0.001;
  pickHelper.pick(pickPosition, scene, camera, time);
  renderer.render(scene, camera);
  requestAnimationFrame(animate);
};

requestAnimationFrame(animate);
