const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(
  50,
  window.innerWidth / window.innerHeight,
  0.1,
  1000
);
const renderer = new THREE.WebGL1Renderer();
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

const geometry = new THREE.BoxGeometry();
const material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
const cube = new THREE.Mesh(geometry, material);
scene.add(cube);

camera.position.z = 5;

let move = 0.1;
function animate() {
  requestAnimationFrame(animate);
  scene.position.x += move;
  if (scene.position.x >= 1) move = -0.1;
  if (scene.position.x <= -1) move = 0.1;
  renderer.render(scene, camera);
}

animate();

// console.log("TRAN BAO KHA");
