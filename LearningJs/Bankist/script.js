"use strict";

/////////////////////////////////////////////////
/////////////////////////////////////////////////
// BANKIST APP

// Data
const account1 = {
  owner: "Jonas Schmedtmann",
  movements: [200, 455.23, -306.5, 25000, -642.21, -133.9, 79.97, 1300, 100],
  interestRate: 1.2, // %
  pin: 1111,

  movementsDates: [
    "2019-11-18T21:31:17.178Z",
    "2019-12-23T07:42:02.383Z",
    "2020-01-28T09:15:04.904Z",
    "2020-04-01T10:17:24.185Z",
    "2020-05-08T14:11:59.604Z",
    "2020-05-27T17:01:17.194Z",
    "2020-07-11T23:36:17.929Z",
    "2020-07-12T10:51:36.790Z",
    "2021-12-20T10:51:36.790Z",
  ],
  currency: "EUR",
  locale: "pt-PT", // de-DE
};

const account2 = {
  owner: "Jessica Davis",
  movements: [5000, 3400, -150, -790, -3210, -1000, 8500, -30],
  interestRate: 1.5,
  pin: 2222,

  movementsDates: [
    "2019-11-01T13:15:33.035Z",
    "2019-11-30T09:48:16.867Z",
    "2019-12-25T06:04:23.907Z",
    "2020-01-25T14:18:46.235Z",
    "2020-02-05T16:33:06.386Z",
    "2020-04-10T14:43:26.374Z",
    "2020-06-25T18:49:59.371Z",
    "2020-07-26T12:01:20.894Z",
  ],
  currency: "USD",
  locale: "en-US",
};

const accounts = [account1, account2];

// Elements
const labelWelcome = document.querySelector(".welcome");
const labelDate = document.querySelector(".date");
const labelBalance = document.querySelector(".balance__value");
const labelSumIn = document.querySelector(".summary__value--in");
const labelSumOut = document.querySelector(".summary__value--out");
const labelSumInterest = document.querySelector(".summary__value--interest");
const labelTimer = document.querySelector(".timer");

const containerApp = document.querySelector(".app");
const containerMovements = document.querySelector(".movements");

const btnLogin = document.querySelector(".login__btn");
const btnTransfer = document.querySelector(".form__btn--transfer");
const btnLoan = document.querySelector(".form__btn--loan");
const btnClose = document.querySelector(".form__btn--close");
const btnSort = document.querySelector(".btn--sort");

const inputLoginUsername = document.querySelector(".login__input--user");
const inputLoginPin = document.querySelector(".login__input--pin");
const inputTransferTo = document.querySelector(".form__input--to");
const inputTransferAmount = document.querySelector(".form__input--amount");
const inputLoanAmount = document.querySelector(".form__input--loan-amount");
const inputCloseUsername = document.querySelector(".form__input--user");
const inputClosePin = document.querySelector(".form__input--pin");

/////////////////////////////////////////////////
/////////////////////////////////////////////////
// LECTURES

const currencies = new Map([
  ["USD", "United States dollar"],
  ["EUR", "Euro"],
  ["GBP", "Pound sterling"],
]);

const movements = [200, 450, -400, 3000, -650, -130, 70, 13000];

const maxMov = movements.reduce(
  (acc, cur) => (cur > acc ? cur : acc),
  movements[0]
);
// console.log(maxMov);

/////////////////////////////////////////////////
const displayCurrentTime = () => {
  const date = new Date();
  const dateOfMonth = date.getDate();
  const month = date.getMonth();
  const year = date.getFullYear();
  const hour = date.getHours();
  const minute = date.getMinutes();
  const currTime = `${dateOfMonth < 10 ? "0" + dateOfMonth : dateOfMonth}/${
    month + 1 < 10 ? "0" + (month + 1) : month + 1
  }/${year}, ${hour < 10 ? "0" + hour : hour}:${
    minute < 10 ? "0" + minute : minute
  }`;
  labelDate.textContent = currTime;
};

const calcDaysPassed = (date) => {
  const day = `${date.getDate()}`.padStart(2, 0);
  const month = `${date.getMonth() + 1}`.padStart(2, 0);
  const year = `${date.getFullYear()}`.padStart(2, 0);
  const daysPassed = Math.floor(
    Math.abs(Number(date) - new Date()) / (1000 * 60 * 60 * 24)
  );
  if (daysPassed === 0) return "Today";
  if (daysPassed === 1) return "Yesterday";
  if (daysPassed <= 7) return `${daysPassed} days ago`;
  return `${day}/${month}/${year}`;
};

let curInterval;
const logoutTimer = () => {
  let timer = 10;

  const tick = () => {
    if (timer === 0) {
      labelWelcome.textContent = "Log in to get started";
      containerApp.style.opacity = 0;
    }
    let hour = `${Math.trunc(timer / 60)}`.padStart(2, 0);
    let minute = `${timer % 60}`.padStart(2, 0);
    labelTimer.textContent = `${hour}:${minute}`;
    timer--;
  };
  tick();
  curInterval = setInterval(tick, 1000);
};

const displayMovements = (account) => {
  containerMovements.innerHTML = "";

  account.movements.forEach((mov, i) => {
    console.log(i);
    const type = mov > 0 ? "deposit" : "withdrawal";
    const date = new Date(account.movementsDates[i]);
    const html = `
      <div class="movements__row">
        <div class="movements__type movements__type--${type}">${
      i + 1
    } ${type}</div>
        <div class="movements__date">${calcDaysPassed(date)}</div>
        <div class="movements__value">${mov.toFixed(2)}€</div>
      </div>
    `;
    containerMovements.insertAdjacentHTML("afterbegin", html);
  });
};

const calcDisplaySummary = ({ movements, interestRate }) => {
  const income = movements
    .filter((mov) => mov > 0)
    .reduce((acc, mov) => acc + mov, 0);
  labelSumIn.textContent = `${income.toFixed(2)}€`;

  const outcome = movements
    .filter((mov) => mov < 0)
    .reduce((acc, mov) => acc + mov, 0);
  labelSumOut.textContent = `${Math.abs(outcome).toFixed(2)}€`;

  const balance = movements.reduce((acc, mov) => acc + mov, 0) * interestRate;
  labelSumInterest.textContent = `${(balance / 100).toFixed(2)}€`;
};

const createUsernames = (accounts) => {
  accounts.forEach((acc) => {
    acc.username = acc.owner
      .toLowerCase()
      .split(" ")
      .map((name) => name[0])
      .join("");
  });
};

createUsernames(accounts);

const calcDisplayBalance = (account) => {
  account.balance = account.movements.reduce((acc, cur) => acc + cur, 0);
  labelBalance.textContent = `${account.balance.toFixed(2)}€`;
};

let account;
btnLogin.addEventListener("click", (e) => {
  e.preventDefault();
  const username = inputLoginUsername.value;
  const pin = Number(inputLoginPin.value);
  account = accounts.find(
    (acc) => acc.username === username && acc.pin === pin
  );
  if (!account) {
    alert("Login failed !!, Wrong username or password");
    return;
  } else {
    inputLoginUsername.value = inputLoginPin.value = "";
    inputLoginPin.blur();
    inputCloseUsername.blur();
    labelWelcome.textContent = `Have a good day, ${
      account.owner.split(" ")[0]
    }!`;
    containerApp.style.opacity = 100;
    displayCurrentTime();
    calcDisplayBalance(account);
    calcDisplaySummary(account);
    displayMovements(account);
    if (curInterval) clearInterval(curInterval);
    logoutTimer();
  }
});

btnTransfer.addEventListener("click", (e) => {
  e.preventDefault();
  const transferTo = inputTransferTo.value;
  const transferAmount = Math.round(+inputTransferAmount.value);
  const accountTransferTo = accounts.find((acc) => acc.username === transferTo);
  if (!accountTransferTo) {
    alert("Transfer to wrong account!");
    return;
  } else {
    if (transferAmount > account.balance) {
      alert("Can not transfer money which is greater than your balance");
      return;
    } else if (transferAmount <= 0) {
      alert("Money transfer have to be greater than 0");
      return;
    } else {
      account.balance -= transferAmount;
      account.movements.push(-transferAmount);
      accountTransferTo.movements.push(transferAmount);
      account.movementsDates.push(new Date().toISOString());
      accountTransferTo.movementsDates.push(new Date().toISOString());
      inputTransferTo.value = inputTransferAmount.value = "";
      inputTransferTo.blur();
      inputTransferAmount.blur();
      calcDisplayBalance(account);
      calcDisplaySummary(account);
      displayMovements(account);
      if (curInterval) clearInterval(curInterval);
      logoutTimer();
    }
  }
});

btnClose.addEventListener("click", (e) => {
  e.preventDefault();
  const username = inputCloseUsername.value;
  const pin = Number(inputClosePin.value);
  if (username === account.username && pin === account.pin) {
    const closedIndex = accounts.findIndex(
      (acc) => acc.username === account.username && acc.pin === account.pin
    );
    accounts.splice(closedIndex, 1);
    containerApp.style.opacity = 0;
    labelWelcome.textContent = "Log in to get started";
  } else {
    inputCloseUsername.value = inputClosePin.value = "";
    inputClosePin.blur();
    inputCloseUsername.blur();
    alert("Wrong username or pin");
    return;
  }
});

btnLoan.addEventListener("click", (e) => {
  e.preventDefault();
  const amount = Math.round(+inputLoanAmount.value);
  if (amount < 0) {
    alert("You have to loan an positive amount");
    return;
  }
  if (account.movements.every((mov) => mov < amount * 0.1)) {
    alert("You can not loan this because of deposit you have");
    return;
  }
  account.movements.push(amount);
  account.movementsDates.push(new Date().toISOString());
  inputLoanAmount.value = "";
  inputLoanAmount.blur();
  calcDisplayBalance(account);
  calcDisplaySummary(account);
  displayMovements(account);
  if (curInterval) clearInterval(curInterval);
  logoutTimer();
});

let sorted = false;
btnSort.addEventListener("click", (e) => {
  e.preventDefault();
  const sortedMovs = account.movements.slice().sort((a, b) => a - b);
  sorted = !sorted;
  if (sorted) {
    displayMovements({
      movements: sortedMovs,
      movementsDates: account.movementsDates,
    });
  } else {
    displayMovements(account);
  }
  calcDisplayBalance(account);
  calcDisplaySummary(account);
});

const allMovsInBank = accounts
  .map((mov) => mov.movements)
  .flat()
  .reduce((acc, mov) => acc + mov, 0);
console.log(allMovsInBank);
