"use strict";

const btn = document.querySelector(".btn-country");
const countriesContainer = document.querySelector(".countries");

///////////////////////////////////////
/*
const renderCountry = function (data, className = "") {
  const html = `
    <article class="country ${className}">
        <img class="country__img" src=${data.flags.png} />
        <div class="country__data">
        <h3 class="country__name">${data.name}</h3>
        <h4 class="country__region">${data.region}</h4>
        <p class="country__row"><span>👫</span>${(
          data.population / 1_000_000
        ).toFixed(2)}M people</p>
        <p class="country__row"><span>🗣️</span>${data.languages[0].name}</p>
        <p class="country__row"><span>💰</span>${data.currencies[0].name}</p>
        </div>
    </article>
`;
  //   console.log(data.languages, data.currencies);
  countriesContainer.insertAdjacentHTML("beforeend", html);
  countriesContainer.style.opacity = 1;
};

const requestCountryAndNeighbor = (country) => {
  const request = new XMLHttpRequest();
  request.open("GET", `https://restcountries.com/v2/name/${country}`);
  request.send();
  request.addEventListener("load", () => {
    const [data] = JSON.parse(request.responseText);
    renderCountry(data);
    // console.log(data);
    data.borders.forEach((code) => {
      const request = new XMLHttpRequest();
      request.open("GET", `https://restcountries.com/v2/alpha/${code}`);
      request.send();
      request.addEventListener("load", () => {
        const data = JSON.parse(request.responseText);
        renderCountry(data, "neighbour");
      });
    });
  });
};

const requestCountryData = function (country) {
  requestCountryAndNeighbor(country);
  countriesContainer.addEventListener("click", (e) => {
    let selectedCountry = e.target.closest(".country")?.textContent;
    if (selectedCountry === undefined) return;
    countriesContainer.innerHTML = "";
    selectedCountry = selectedCountry.trim();
    selectedCountry = selectedCountry.slice(0, selectedCountry.indexOf(" "));
    requestCountryAndNeighbor(selectedCountry);
  });
};

requestCountryData("vietnam");
// requestCountryData("nigeria");
// requestCountryData("vietnam");
*/
/////////////////////////////////////////////////
const renderCountry = function (data, className = "") {
  const html = `
    <article class="country ${className}">
        <img class="country__img" src=${data.flags.png} />
        <div class="country__data">
        <h3 class="country__name">${data.name}</h3>
        <h4 class="country__region">${data.region}</h4>
        <p class="country__row"><span>👫</span>${(
          data.population / 1_000_000
        ).toFixed(2)}M people</p>
        <p class="country__row"><span>🗣️</span>${data.languages[0].name}</p>
        <p class="country__row"><span>💰</span>${data.currencies[0].name}</p>
        </div>
    </article>
`;
  //   console.log(data.languages, data.currencies);
  countriesContainer.insertAdjacentHTML("beforeend", html);
};

const displayErr = function (err) {
  countriesContainer.insertAdjacentText("beforeend", err.message);
};

const requestCountryData = function (country) {
  fetch(`https://restcountries.com/v2/name/${country}`)
    .then((res) => res.json())
    .then((data) => {
      if (data[0] === undefined) throw new Error("Country can not be Found");
      renderCountry(data[0]);
      data[0].borders?.forEach((code) => {
        fetch(`https://restcountries.com/v2/alpha/${code}`)
          .then((res) => res.json())
          .then((data) => {
            // console.log(data);
            renderCountry(data, "neighbour");
          });
      });
    })
    .catch((err) => displayErr(err))
    .finally(() => (countriesContainer.style.opacity = 1));
};

const requestCountryAndNeighbor = function (country) {
  requestCountryData(country);
  countriesContainer.addEventListener("click", (e) => {
    let selectedCountry = e.target.closest(".country")?.textContent;
    if (selectedCountry === undefined) return;
    countriesContainer.innerHTML = "";
    selectedCountry = selectedCountry.trim();
    selectedCountry = selectedCountry.slice(0, selectedCountry.indexOf(" "));
    requestCountryData(selectedCountry);
  });
};

const displayCurrCountry = function () {
  let lat, lng;
  navigator.geolocation.getCurrentPosition((position) => {
    lat = position.coords.latitude;
    lng = position.coords.longitude;
    console.log(lat, lng);
    fetch(`https://geocode.xyz/${lat},${lng}?geoit=json`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data.country);
        requestCountryAndNeighbor(data.country);
      });
  });
};
// displayCurrCountry();
requestCountryAndNeighbor("vietnam");
